## Please update files in main.tf

```sh

resource "fortios_firewall_policy" "f5_mytsel_nomad" {
  action             = "accept"
  logtraffic         = "utm"
  name               = "f5_mytsel_nomad"
  policyid           =  13
  schedule           = "always"
  wanopt             = "disable"
  wanopt_detection   = "active"
  wanopt_passive_opt = "default"
  wccp               = "disable"
  webcache           = "disable"
  webcache_https     = "disable"
  wsso               = "enable"


  srcaddr {
    name = "addr_f5_mytsel"
  }

  srcintf {
    name = "port1"
  }

  dstaddr {
    name = "group_f5_mytsel_nomad"
  }

  dstintf {
    name = "port2"
  }

  service {
    name = "HTTP"
  }
}



resource "fortios_firewall_policy" "f5_nomad_esb" {
  action             = "accept"
  logtraffic         = "utm"
  name               = "f5_nomad_esb"
  policyid           =  14
  schedule           = "always"
  wanopt             = "disable"
  wanopt_detection   = "active"
  wanopt_passive_opt = "default"
  wccp               = "disable"
  webcache           = "disable"
  webcache_https     = "disable"
  wsso               = "enable"


  srcaddr {
    name = "group_f5_nomad_esb"
  }

  srcintf {
    name = "port2"
  }

  dstaddr {
    name = "addr_f5_esb"
  }

  dstintf {
    name = "port1"
  }

  service {
    name = "HTTP"
  }
}
```